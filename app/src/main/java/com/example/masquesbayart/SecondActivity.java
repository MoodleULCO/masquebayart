package com.example.masquesbayart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    // Constantes Extra
    public static final String NAME_EDITED = "com.example.testIntent03.NAME_EDITED";
    public static final String FIRST_NAME_EDITED = "com.example.testIntent03.FIRST_NAME_EDITED";
    public static final String PHONE_EDITED = "com.example.testIntent03.PHONE_EDITED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        updateInputs();

        final Button validate = (Button) findViewById(R.id.validateButton1);
        validate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Lors du clic sur le bouton "Valider"
                sendMessage(v);
            }
        });

        final Button cancel = (Button) findViewById(R.id.cancelButton1);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Lors du clic sur le bouton "Annuler"
                cancel(v);
            }
        });
    }

    /**
     * Méthode utilisée pour l'envoi des informations modifiées à la MainActivity
     * @param view
     */
    private void sendMessage(View view) {
        EditText nameInput = findViewById(R.id.nameInput);
        EditText firstNameInput = findViewById(R.id.firstNameInput);
        EditText phoneInput = findViewById(R.id.phoneInput);

        // On récupère les valeurs saisies dans les champs de saisie de texte
        String nameInputStr = nameInput.getText().toString();
        String firstNameInputStr = firstNameInput.getText().toString();
        String phoneInputStr = phoneInput.getText().toString();

        // On envoie la chaîne passée en paramètre à l'activité principale
        Intent intent = new Intent();
        intent.putExtra(NAME_EDITED, nameInputStr);
        intent.putExtra(FIRST_NAME_EDITED, firstNameInputStr);
        intent.putExtra(PHONE_EDITED, phoneInputStr);

        // On déclare la modification afin de déclencher le onActivityResult
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Méthode utilisée pour annuler les modifications et revenir a la MainActivity
     * @param view
     */
    private void cancel(View view) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    /**
     * Méthode permettant de récupérer les valeurs de la MainActivity et mettre à jour les champs de l'activité courante
     */
    private void updateInputs() {
        // On récupère la valeur courrante du champs de saisie de l'activité principale
        String nameStr = getIntent().getStringExtra(MainActivity.NAME);
        String firstNameStr = getIntent().getStringExtra(MainActivity.FIRST_NAME);
        String phoneStr = getIntent().getStringExtra(MainActivity.PHONE);

        TextView name = findViewById(R.id.nameInput);
        TextView firstName = findViewById(R.id.firstNameInput);
        TextView phone = findViewById(R.id.phoneInput);

        // On affecte la valeur récupérée précédemment au champs de texte
        name.setText(nameStr);
        firstName.setText(firstNameStr);
        phone.setText(phoneStr);
    }
}