package com.example.masquesbayart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {
    // Constantes Extra
    public static final String ADDRESS_NUMBER_EDITED = "com.example.testIntent03.ADDRESS_NUMBER_EDITED";
    public static final String ADDRESS_NAME_EDITED = "com.example.testIntent03.ADDRESS_NAME_EDITED";
    public static final String POSTAL_CODE_EDITED = "com.example.testIntent03.POSTAL_CODE_EDITED";
    public static final String CITY_EDITED = "com.example.testIntent03.CITY_EDITED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        updateInputs();

        final Button validate = (Button) findViewById(R.id.validateButton2);
        validate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Lors du clic sur le bouton "Valider"
                sendMessage(v);
            }
        });

        final Button cancel = (Button) findViewById(R.id.cancelButton2);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Lors du clic sur le bouton "Annuler"
                cancel(v);
            }
        });
    }

    /**
     * Méthode utilisée pour l'envoi des informations modifiées à la MainActivity
     * @param view
     */
    private void sendMessage(View view) {
        // On récupère les valeurs saisies dans les champs de saisie de texte
        TextView addressNumber = findViewById(R.id.addressNumberInput);
        TextView addressName = findViewById(R.id.addressInput);
        TextView postalCode = findViewById(R.id.cpInput);
        TextView city = findViewById(R.id.cityInput);

        String addressNumberStr = addressNumber.getText().toString();
        String addressNameStr = addressName.getText().toString();
        String postalCodeStr = postalCode.getText().toString();
        String cityStr = city.getText().toString();

        // On envoie la chaîne passée en paramètre à l'activité principale
        Intent intent = new Intent();
        intent.putExtra(ADDRESS_NUMBER_EDITED, addressNumberStr);
        intent.putExtra(ADDRESS_NAME_EDITED, addressNameStr);
        intent.putExtra(POSTAL_CODE_EDITED, postalCodeStr);
        intent.putExtra(CITY_EDITED, cityStr);

        // On déclare la modification afin de déclencher le onActivityResult
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Méthode utilisée pour annuler les modifications et revenir a la MainActivity
     * @param view
     */
    private void cancel(View view) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    /**
     * Méthode permettant de récupérer les valeurs de la MainActivity et mettre à jour les champs de l'activité courante
     */
    private void updateInputs() {
        // On récupère la valeur courrante du champs de saisie de l'activité principale
        String addressNumberStr = getIntent().getStringExtra(MainActivity.ADDRESS_NUMBER);
        String addressNameStr = getIntent().getStringExtra(MainActivity.ADDRESS_NAME);
        String postalCodeStr = getIntent().getStringExtra(MainActivity.POSTAL_CODE);
        String cityStr = getIntent().getStringExtra(MainActivity.CITY);

        TextView addressNumber = findViewById(R.id.addressNumberInput);
        TextView addressName = findViewById(R.id.addressInput);
        TextView postalCode = findViewById(R.id.cpInput);
        TextView city = findViewById(R.id.cityInput);

        // On affecte la valeur récupérée précédemment au champs de texte
        addressNumber.setText(addressNumberStr);
        addressName.setText(addressNameStr);
        postalCode.setText(postalCodeStr);
        city.setText(cityStr);
    }
}