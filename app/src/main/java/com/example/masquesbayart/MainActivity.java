package com.example.masquesbayart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    // Constantes Extra
    public static final String NAME = "com.example.testIntent03.NAME";
    public static final String FIRST_NAME = "com.example.testIntent03.FIRST_NAME";
    public static final String PHONE = "com.example.testIntent03.PHONE";

    public static final String ADDRESS_NUMBER = "com.example.testIntent03.ADDRESS_NUMBER";
    public static final String ADDRESS_NAME = "com.example.testIntent03.ADDRESS_NAME";
    public static final String POSTAL_CODE = "com.example.testIntent03.POSTAL_CODE";
    public static final String CITY = "com.example.testIntent03.CITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Button edit1 = (Button) findViewById(R.id.editButton1);
        edit1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Appel à editIdentityBloc lors du clic sur le bouton "Modifier" du bloc identité
                editIdentityBloc(v);
            }
        });

        final Button edit2 = (Button) findViewById(R.id.editButton2);
        edit2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Appel à editAdressBloc lors du clic sur le bouton "Modifier" du bloc adresse
                editAdressBloc(v);
            }
        });
    }

    /**
     * Méthode permettant de démarrer la SecondActivity
     * @param view
     */
    public void editIdentityBloc(View view) {
        Intent intent = new Intent(this, SecondActivity.class);

        TextView name = findViewById(R.id.name);
        TextView firstName = findViewById(R.id.firstName);
        TextView phone = findViewById(R.id.phone);

        // Récupération des valeurs courantes des champs de texte
        String nameStr = name.getText().toString();
        String firstNameStr = firstName.getText().toString();
        String phoneStr = phone.getText().toString();

        // Envoie des données actuelles a l'activité secondaire
        intent.putExtra(NAME, nameStr);
        intent.putExtra(FIRST_NAME, firstNameStr);
        intent.putExtra(PHONE, phoneStr);

        // Démarrage de l'activité 2
        startActivityForResult(intent, 1);
    }

    /**
     * Méthode permettant de démarrer la ThirdActivity
     * @param view
     */
    public void editAdressBloc(View view) {
        Intent intent = new Intent(this, ThirdActivity.class);

        TextView addressNumber = findViewById(R.id.addressNumberInput);
        TextView addressName = findViewById(R.id.addressInput);
        TextView postalCode = findViewById(R.id.postalCode);
        TextView city = findViewById(R.id.cityInput);

        // Récupération des valeurs courantes des champs de texte
        String addressNumberStr = addressNumber.getText().toString();
        String addressNameStr = addressName.getText().toString();
        String postalCodeStr = postalCode.getText().toString();
        String cityStr = city.getText().toString();

        // Envoie des données actuelles a l'activité secondaire
        intent.putExtra(ADDRESS_NUMBER, addressNumberStr);
        intent.putExtra(ADDRESS_NAME, addressNameStr);
        intent.putExtra(POSTAL_CODE, postalCodeStr);
        intent.putExtra(CITY, cityStr);

        // Démarrage de l'activité 2
        startActivityForResult(intent, 2);
    }

    /**
     * Méthode permettant de "capturer" une modification dans une sous activité
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Lors de la validation par l'activité 2 :
        if(requestCode == 1 && resultCode == RESULT_OK && data != null) {
            // Modification du champs de text avec la valeur saisie
            String editedName = data.getStringExtra(SecondActivity.NAME_EDITED);
            String editedFirstName = data.getStringExtra(SecondActivity.FIRST_NAME_EDITED);
            String editedPhone = data.getStringExtra(SecondActivity.PHONE_EDITED);

            TextView name = findViewById(R.id.name);
            TextView firstName = findViewById(R.id.firstName);
            TextView phone = findViewById(R.id.phone);

            // Affectation des nouvelles valeurs aux champs de texte
            name.setText(editedName);
            firstName.setText(editedFirstName);
            phone.setText(editedPhone);
        }

        // Lors de la validation par l'activité 3 :
        if(requestCode == 2 && resultCode == RESULT_OK && data != null) {
            // Modification du champs de text avec la valeur saisie
            String editedAddressNumber = data.getStringExtra(ThirdActivity.ADDRESS_NUMBER_EDITED);
            String editedAddressName = data.getStringExtra(ThirdActivity.ADDRESS_NAME_EDITED);
            String editedPostalCode = data.getStringExtra(ThirdActivity.POSTAL_CODE_EDITED);
            String editedCity = data.getStringExtra(ThirdActivity.CITY_EDITED);

            TextView addressNumber = findViewById(R.id.addressNumberInput);
            TextView addressName = findViewById(R.id.addressInput);
            TextView postalCode = findViewById(R.id.postalCode);
            TextView city = findViewById(R.id.cityInput);

            // Affectation des nouvelles valeurs aux champs de texte
            addressNumber.setText(editedAddressNumber);
            addressName.setText(editedAddressName);
            postalCode.setText(editedPostalCode);
            city.setText(editedCity);
        }
    }
}